# Heracles Tournament Season Manager
Software to create, generate, track and operate tournaments and seasons for competitive events.

In the early iterations, users will be able to implement all game/sports that declares a winner by having the most points at the end of the game without many issues. I plan to have multiple options for winning conditions such as highest or lowest number of points.

Also, teams are currently ordered by number of points, offering 3 points for a win, 1 for at tie and 0 for a loss. This is primarily the football method of scoring across a season but there will be other implementations in the future such as 2 points for wins and tie-less games(sorting by wins rather than points).
