﻿using System.Collections.Generic;

namespace HeraclesTSM.Model
{
    internal class Tournament
    {
        #region Properties

        public string Name { get; set; }
        public List<Team> Teams { get; }
        public List<Match> Matches { get; }
        public bool DoubleElimination { get; set; }
        public int NumberOfTeams { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tournament" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="doubleElimination">if set to <c>true</c> [double elimination].</param>
        public Tournament(string name, bool doubleElimination)
        {
            this.Name = name;
            this.DoubleElimination = doubleElimination;
            this.Teams = new List<Team>();
            this.Matches = new List<Match>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tournament" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="doubleElimination">if set to <c>true</c> [double elimination].</param>
        /// <param name="numberOfTeams">The number of teams.</param>
        public Tournament(string name, bool doubleElimination, int numberOfTeams)
        {
            this.Name = name;
            this.DoubleElimination = doubleElimination;
            this.NumberOfTeams = numberOfTeams;
            this.Teams = new List<Team>();
            this.Matches = new List<Match>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Tournament" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="doubleElimination">if set to <c>true</c> [double elimination].</param>
        /// <param name="teams">The teams.</param>
        public Tournament(string name, bool doubleElimination, List<Team> teams)
        {
            this.Name = name;
            this.DoubleElimination = doubleElimination;
            this.Teams = teams;
            this.Matches = new List<Match>();
        }

        #endregion
    }
}