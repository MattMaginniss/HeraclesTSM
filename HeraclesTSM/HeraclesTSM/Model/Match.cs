﻿using System;

namespace HeraclesTSM.Model
{
    [Serializable]
    public class Match
    {
        #region Properties

        public int MatchNumber { get; private set; }
        public Team HomeTeam { get; }
        public Team AwayTeam { get; }
        public int HomeTeamScore { get; set; }
        public int AwayTeamScore { get; set; }
        public DateTime DateTime { get; private set; }
        public bool IsFinished { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Match" /> class.
        /// </summary>
        /// <param name="homeTeam">The home team.</param>
        /// <param name="awayTeam">The away team.</param>
        /// <param name="number">The number.</param>
        /// <exception cref="System.ArgumentException">Input teams cannot be null.</exception>
        public Match(Team homeTeam, Team awayTeam, int number)
        {
            if (homeTeam == null || awayTeam == null)
            {
                throw new ArgumentException("Input teams cannot be null.");
            }
            if (homeTeam == awayTeam)
            {
                throw new ArgumentException("A team cannot play itself.");
            }

            this.MatchNumber = number;
            this.HomeTeam = homeTeam;
            this.HomeTeamScore = 0;
            this.AwayTeam = awayTeam;
            this.AwayTeamScore = 0;
            this.DateTime = DateTime.MinValue;
            this.IsFinished = false;
        }

        #endregion

        /// <summary>
        ///     Inputs the game score
        /// </summary>
        /// <param name="homeTeamScore">The home team score.</param>
        /// <param name="awayTeamScore">The away team score.</param>
        /// <exception cref="ArgumentException">Score cannot be less than 0</exception>
        public void InputGameScore(int homeTeamScore, int awayTeamScore)
        {
            if (homeTeamScore < 0 || awayTeamScore < 0)
            {
                throw new ArgumentException("Score cannot be less than 0");
            }
            this.HomeTeamScore = homeTeamScore;
            this.AwayTeamScore = awayTeamScore;
            this.IsFinished = true;

            this.calculateWinnerAndUpdateStats();
        }

        /// <summary>
        ///     Sets the date.
        /// </summary>
        /// <param name="matchDateTime">The match date time.</param>
        public void SetDate(DateTime matchDateTime)
        {
            this.DateTime = matchDateTime;
        }

        /// <summary>
        ///     Resets the match.
        /// </summary>
        public void ResetMatch()
        {
            if (this.HomeTeamScore > this.AwayTeamScore)
            {
                this.HomeTeam.Wins -= 1;
                this.HomeTeam.Points -= 3;
                this.AwayTeam.Losses -= 1;
            }
            else if (this.AwayTeamScore > this.HomeTeamScore)
            {
                this.AwayTeam.Wins -= 1;
                this.AwayTeam.Points -= 3;
                this.HomeTeam.Losses -= 1;
            }
            else
            {
                this.HomeTeam.Ties -= 1;
                this.HomeTeam.Points -= 1;
                this.AwayTeam.Ties -= 1;
                this.AwayTeam.Points -= 1;
            }

            this.HomeTeamScore = 0;
            this.AwayTeamScore = 0;
            this.IsFinished = false;
        }

        private void calculateWinnerAndUpdateStats()
        {
            if (this.HomeTeamScore > this.AwayTeamScore)
            {
                this.HomeTeam.Win(this.HomeTeamScore, this.AwayTeamScore);
                this.AwayTeam.Loss(this.AwayTeamScore, this.HomeTeamScore);
            }
            else if (this.AwayTeamScore > this.HomeTeamScore)
            {
                this.AwayTeam.Win(this.AwayTeamScore, this.HomeTeamScore);
                this.HomeTeam.Loss(this.HomeTeamScore, this.AwayTeamScore);
            }
            else
            {
                this.AwayTeam.Tie(this.AwayTeamScore, this.HomeTeamScore);
                this.HomeTeam.Tie(this.HomeTeamScore, this.AwayTeamScore);
            }
        }

        /// <summary>
        ///     Updates the match number.
        /// </summary>
        /// <param name="matchNumber">The match number.</param>
        public void UpdateMatchNumber(int matchNumber)
        {
            this.MatchNumber = matchNumber;
        }

        public override string ToString()
        {
            return this.HomeTeam.Name + " " + this.HomeTeamScore + " - " + this.AwayTeamScore + " " + this.AwayTeam.Name;
        }
    }
}