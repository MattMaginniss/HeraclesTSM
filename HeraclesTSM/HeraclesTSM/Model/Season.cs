﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HeraclesTSM.Model
{
    [Serializable]
    public class Season
    {
        #region Properties

        public string Name { get; set; }
        public List<Team> Teams { get; }
        public List<Team> TeamStandings { get; private set; }
        public List<Match> Matches { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Season" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Season(string name)
        {
            this.Name = name;
            this.Teams = new List<Team>();
            this.Matches = new List<Match>();
        }

        #endregion

        /// <summary>
        ///     Adds the team to tournament.
        /// </summary>
        /// <param name="team">The team.</param>
        public void AddTeamToSeason(Team team)
        {
            foreach (Team originalTeam in this.Teams)
            {
                if (originalTeam.Name == team.Name)
                {
                    throw new ArgumentException("You cannot have two teams with the same name.");
                }
            }
            this.Teams.Add(team);
        }

        /// <summary>
        ///     Creates the match.
        /// </summary>
        /// <param name="homeTeam">The home team.</param>
        /// <param name="awayTeam">The away team.</param>
        public void CreateMatch(Team homeTeam, Team awayTeam)
        {
            var matchNumber = this.Matches.Count + 1;
            this.Matches.Add(new Match(homeTeam, awayTeam, matchNumber));
        }

        public void AddMatch(Match newMatch)
        {
            this.Matches.Add(newMatch);
        }

        /// <summary>
        ///     Plays the match.
        /// </summary>
        /// <param name="matchIndex">Index of the match.</param>
        /// <param name="homeTeamScore">The home team score.</param>
        /// <param name="awayTeamScore">The away team score.</param>
        /// <exception cref="ArgumentException">The match index is invalid.</exception>
        public void PlayMatch(int matchIndex, int homeTeamScore, int awayTeamScore)
        {
            if (matchIndex < 0 || matchIndex > this.Matches.Count)
            {
                throw new ArgumentException("The match index is invalid.");
            }
            this.Matches[matchIndex].InputGameScore(homeTeamScore, awayTeamScore);
            this.SortStandings();
        }

        /// <summary>
        ///     Sorts the team standings by the number of wins.
        /// </summary>
        public void SortStandings()
        {
            var standings =
                this.Teams.OrderBy(team => team.Points).ThenBy(team => team.PointDifference).Reverse().ToList();
            this.TeamStandings = standings;
        }

        /// <summary>
        ///     Gets a team from the list.
        /// </summary>
        /// <param name="index">The index of the team.</param>
        /// <returns>a team at the entered index.</returns>
        public Team GetTeam(int index)
        {
            return this.Teams[index];
        }

        /// <summary>
        ///     Gets the match.
        /// </summary>
        /// <param name="index">The index of the match.</param>
        /// <returns>The match at index.</returns>
        public Match GetMatch(int index)
        {
            return this.Matches[index];
        }

        /// <summary>
        ///     Edits the team name.
        /// </summary>
        /// <param name="teamIndex">Index of the team.</param>
        /// <param name="newTeamName">New name of the team.</param>
        public void EditTeamName(int teamIndex, string newTeamName)
        {
            this.Teams[teamIndex].SetTeamName(newTeamName);
        }

        /// <summary>
        ///     Deletes the match at the specified index.
        /// </summary>
        /// <param name="matchIndex">Index of the match.</param>
        public void DeleteMatch(int matchIndex)
        {
            this.Matches.Remove(this.Matches[matchIndex]);
            var matchNumber = 1;

            foreach (var match in this.Matches)
            {
                match.UpdateMatchNumber(matchNumber);
                matchNumber++;
            }
        }

        public new string ToString()
        {
            var i = 1;
            var standings = this.Name + " Standings:\n";
            foreach (var team in this.TeamStandings)
            {
                standings += i + ": " + team + "\n";
                i ++;
            }

            return standings;
        }
    }
}