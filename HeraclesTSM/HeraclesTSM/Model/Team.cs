﻿using System;

namespace HeraclesTSM.Model
{
    [Serializable]
    public class Team
    {
        #region Properties

        public string Name { get; private set; }
        public int GamesPlayed { get; private set; }
        public int Wins { get; set; }
        public int Ties { get; set; }
        public int Losses { get; set; }

        public int PointDifference { get; private set; }
        public int PointsFor { get; private set; }
        public int PointsAgainst { get; private set; }

        public int Points { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Team" /> class.
        /// </summary>
        /// <param name="teamName">Name of the team.</param>
        public Team(string teamName)
        {
            this.Name = teamName;
            this.GamesPlayed = 0;
            this.Wins = 0;
            this.Ties = 0;
            this.Losses = 0;
            this.PointDifference = 0;
            this.PointsFor = 0;
            this.PointsAgainst = 0;
            this.Points = 0;
        }

        #endregion

        /// <summary>
        ///     Wins the specified points for.
        /// </summary>
        /// <param name="pointsFor">The points for.</param>
        /// <param name="pointsAgainst">The points against.</param>
        public void Win(int pointsFor, int pointsAgainst)
        {
            this.Wins++;
            this.Points += 3;
            this.addPointsForAndAgainst(pointsFor, pointsAgainst);
            this.GamesPlayed++;
        }

        /// <summary>
        ///     Losses the specified points for.
        /// </summary>
        /// <param name="pointsFor">The points for.</param>
        /// <param name="pointsAgainst">The points against.</param>
        public void Loss(int pointsFor, int pointsAgainst)
        {
            this.Losses++;
            this.addPointsForAndAgainst(pointsFor, pointsAgainst);
            this.GamesPlayed++;
        }

        /// <summary>
        ///     Ties the specified points for.
        /// </summary>
        /// <param name="pointsFor">The points for.</param>
        /// <param name="pointsAgainst">The points against.</param>
        public void Tie(int pointsFor, int pointsAgainst)
        {
            this.Ties++;
            this.Points += 1;
            this.addPointsForAndAgainst(pointsFor, pointsAgainst);
            this.GamesPlayed++;
        }

        private void addPointsForAndAgainst(int pointsFor, int pointsAgainst)
        {
            this.PointsFor += pointsFor;
            this.PointsAgainst += pointsAgainst;
            this.PointDifference = this.PointsFor - this.PointsAgainst;
        }

        /// <summary>
        ///     Sets the name of the team.
        /// </summary>
        /// <param name="teamName">Name of the team.</param>
        public void SetTeamName(string teamName)
        {
            this.Name = teamName;
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents the team object.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}