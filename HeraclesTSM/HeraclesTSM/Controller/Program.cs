using System;
using System.Windows.Forms;
using HeraclesTSM.View.Windows;

namespace TournamentSoftware.Controller
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        // ReSharper disable once InconsistentNaming 'Main' is necessary
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PrimaryWindow());

            //            var tournament = new Tournament("CSGO");
            //            var tsm = new Team("TSM");
            //            var fnatic = new Team("fnatic");
            //            var vp = new Team("VP");
            //            var envyus = new Team("EnVyUs");
            //            tournament.AddTeamToSeason(tsm);
            //            tournament.AddTeamToSeason(fnatic);
            //            tournament.AddTeamToSeason(vp);
            //            tournament.AddTeamToSeason(envyus);
            //            tournament.CreateMatch(tsm, fnatic);
            //            tournament.CreateMatch(vp, envyus);
            //            tournament.CreateMatch(tsm, vp);
            //            tournament.CreateMatch(fnatic, envyus);
            //            tournament.CreateMatch(tsm, envyus);
            //            tournament.CreateMatch(fnatic, vp);
            //
            //            tournament.PlayMatch(0, 16, 12);
            //            tournament.PlayMatch(1, 12, 16);
            //            tournament.PlayMatch(2, 11, 16);
            //            tournament.PlayMatch(3, 16, 9);
            //            tournament.PlayMatch(4, 16, 11);
            //            tournament.PlayMatch(5, 16, 21);
            //
            //            tournament.SortStandings();
            //            Console.WriteLine(tournament.ToString());
        }
    }
}