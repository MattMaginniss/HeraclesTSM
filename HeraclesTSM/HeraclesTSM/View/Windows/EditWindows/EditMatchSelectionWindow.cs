﻿using System;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.EditWindows
{
    public partial class EditMatchSelectionWindow : Form
    {
        #region Data members

        private readonly Season season;

        #endregion

        #region Constructors

        public EditMatchSelectionWindow(Season season)
        {
            this.InitializeComponent();
            this.season = season;

            this.updateEditMatchesView();
        }

        #endregion

        private void updateEditMatchesView()
        {
            this.dgvEditMatches.Rows.Clear();

            foreach (var match in this.season.Matches)
            {
                var matchNumber = match.MatchNumber;
                var homeTeam = match.HomeTeam.Name;
                var scoreTime = this.setScoreTime(match);
                if (match.IsFinished)
                {
                    scoreTime = match.HomeTeamScore + " - " + match.AwayTeamScore;
                }
                var awayTeam = match.AwayTeam.Name;

                this.dgvEditMatches.Rows.Add(matchNumber, homeTeam, scoreTime, awayTeam);
            }
            this.dgvEditMatches.Show();
        }

        private string setScoreTime(Match match)
        {
            string scoreTime;
            if (match.IsFinished)
            {
                scoreTime = match.HomeTeamScore + " - " + match.AwayTeamScore;
            }
            else if (match.DateTime == System.DateTime.MinValue)
            {
                scoreTime = "Match Date Not Set";
            }
            else
            {
                scoreTime = match.DateTime.Day + "/" + match.DateTime.Month + "/" + match.DateTime.Year + " @ " +
                            $"{match.DateTime.Hour:00}" + ":" + $"{match.DateTime.Minute:00}";
            }
            return scoreTime;
        }

        private void dgvEditMatches_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var matchSelection = this.dgvEditMatches.CurrentCell.RowIndex;

            var editMatchEditWindow = new EditMatchEditWindow(this.season, matchSelection);

            editMatchEditWindow.ShowDialog();

            editMatchEditWindow.Close();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}