﻿using System;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.EditWindows
{
    public partial class PlayMatchWindow : Form
    {
        #region Properties

        public int HomeTeamScore { get; private set; }
        public int AwayTeamScore { get; private set; }

        #endregion

        #region Constructors

        public PlayMatchWindow(Match match)
        {
            this.InitializeComponent();
            this.lblHomeTeamName.Text = match.HomeTeam.Name;
            this.lblAwayTeamName.Text = match.AwayTeam.Name;
        }

        #endregion

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.HomeTeamScore = (int) this.numHomeScore.Value;
            this.AwayTeamScore = (int) this.numAwayScore.Value;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}