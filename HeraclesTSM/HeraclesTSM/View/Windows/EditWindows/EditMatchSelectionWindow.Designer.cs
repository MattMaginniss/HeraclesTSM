﻿namespace HeraclesTSM.View.Windows.EditWindows
{
    partial class EditMatchSelectionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvEditMatches = new System.Windows.Forms.DataGridView();
            this.MatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HomeTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AwayTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDone = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditMatches)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEditMatches
            // 
            this.dgvEditMatches.AllowUserToAddRows = false;
            this.dgvEditMatches.AllowUserToDeleteRows = false;
            this.dgvEditMatches.AllowUserToResizeColumns = false;
            this.dgvEditMatches.AllowUserToResizeRows = false;
            this.dgvEditMatches.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEditMatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEditMatches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MatchNumber,
            this.HomeTeam,
            this.DateTime,
            this.AwayTeam});
            this.dgvEditMatches.Location = new System.Drawing.Point(12, 12);
            this.dgvEditMatches.Name = "dgvEditMatches";
            this.dgvEditMatches.ReadOnly = true;
            this.dgvEditMatches.RowHeadersVisible = false;
            this.dgvEditMatches.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvEditMatches.Size = new System.Drawing.Size(367, 259);
            this.dgvEditMatches.TabIndex = 0;
            this.dgvEditMatches.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEditMatches_CellContentClick);
            // 
            // MatchNumber
            // 
            this.MatchNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.MatchNumber.HeaderText = "#";
            this.MatchNumber.Name = "MatchNumber";
            this.MatchNumber.ReadOnly = true;
            this.MatchNumber.Width = 39;
            // 
            // HomeTeam
            // 
            this.HomeTeam.HeaderText = "Home Team";
            this.HomeTeam.Name = "HomeTeam";
            this.HomeTeam.ReadOnly = true;
            this.HomeTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DateTime
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DateTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.DateTime.HeaderText = "Time/Result";
            this.DateTime.Name = "DateTime";
            this.DateTime.ReadOnly = true;
            this.DateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AwayTeam
            // 
            this.AwayTeam.HeaderText = "Away Team";
            this.AwayTeam.Name = "AwayTeam";
            this.AwayTeam.ReadOnly = true;
            this.AwayTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(150, 295);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 1;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // EditMatchSelectionWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 346);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.dgvEditMatches);
            this.Name = "EditMatchSelectionWindow";
            this.Text = "Select Match to Edit";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditMatches)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEditMatches;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatchNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn HomeTeam;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AwayTeam;
        private System.Windows.Forms.Button btnDone;
    }
}