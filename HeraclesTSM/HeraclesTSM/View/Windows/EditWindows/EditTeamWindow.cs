﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.EditWindows
{
    public partial class EditTeamWindow : Form
    {
        #region Data members

        private readonly Season season;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EditTeamWindow" /> class.
        /// </summary>
        /// <param name="season">The season.</param>
        public EditTeamWindow(Season season)
        {
            this.InitializeComponent();
            this.season = season;

            var teamSource = new Dictionary<int, string>();
            foreach (var team in this.season.Teams)
            {
                teamSource.Add(this.season.Teams.IndexOf(team), team.Name);
            }
            this.drpDwnTeamName.DataSource = new BindingSource(teamSource, null);
            this.drpDwnTeamName.DisplayMember = "Value";
        }

        #endregion

        private void btnEnter_Click(object sender, EventArgs e)
        {
            this.season.EditTeamName(this.drpDwnTeamName.SelectedIndex, this.txtNewTeamName.Text);
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}