﻿namespace HeraclesTSM.View.Windows.EditWindows
{
    partial class EditMatchEditWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHomeTeamName = new System.Windows.Forms.Label();
            this.lblHomeTeam = new System.Windows.Forms.Label();
            this.lblAwayTeamName = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.numAwayScore = new System.Windows.Forms.NumericUpDown();
            this.numHomeScore = new System.Windows.Forms.NumericUpDown();
            this.lblAwayTeam = new System.Windows.Forms.Label();
            this.radioEditMatch = new System.Windows.Forms.RadioButton();
            this.radioResetMatch = new System.Windows.Forms.RadioButton();
            this.radioDeleteMatch = new System.Windows.Forms.RadioButton();
            this.chkDateTime = new System.Windows.Forms.CheckBox();
            this.matchTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblTimeAt = new System.Windows.Forms.Label();
            this.matchDateTimePicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.numAwayScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeScore)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHomeTeamName
            // 
            this.lblHomeTeamName.AutoSize = true;
            this.lblHomeTeamName.Location = new System.Drawing.Point(26, 118);
            this.lblHomeTeamName.Name = "lblHomeTeamName";
            this.lblHomeTeamName.Size = new System.Drawing.Size(0, 13);
            this.lblHomeTeamName.TabIndex = 19;
            // 
            // lblHomeTeam
            // 
            this.lblHomeTeam.AutoSize = true;
            this.lblHomeTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblHomeTeam.Location = new System.Drawing.Point(23, 98);
            this.lblHomeTeam.Name = "lblHomeTeam";
            this.lblHomeTeam.Size = new System.Drawing.Size(93, 16);
            this.lblHomeTeam.TabIndex = 18;
            this.lblHomeTeam.Text = "Home Team";
            // 
            // lblAwayTeamName
            // 
            this.lblAwayTeamName.AutoSize = true;
            this.lblAwayTeamName.Location = new System.Drawing.Point(26, 59);
            this.lblAwayTeamName.Name = "lblAwayTeamName";
            this.lblAwayTeamName.Size = new System.Drawing.Size(0, 13);
            this.lblAwayTeamName.TabIndex = 17;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(196, 302);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(19, 303);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(92, 23);
            this.btnAccept.TabIndex = 15;
            this.btnAccept.Text = "Submit";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click_1);
            // 
            // numAwayScore
            // 
            this.numAwayScore.Location = new System.Drawing.Point(221, 51);
            this.numAwayScore.Name = "numAwayScore";
            this.numAwayScore.Size = new System.Drawing.Size(53, 20);
            this.numAwayScore.TabIndex = 14;
            // 
            // numHomeScore
            // 
            this.numHomeScore.Location = new System.Drawing.Point(221, 111);
            this.numHomeScore.Name = "numHomeScore";
            this.numHomeScore.Size = new System.Drawing.Size(53, 20);
            this.numHomeScore.TabIndex = 13;
            // 
            // lblAwayTeam
            // 
            this.lblAwayTeam.AutoSize = true;
            this.lblAwayTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblAwayTeam.Location = new System.Drawing.Point(23, 39);
            this.lblAwayTeam.Name = "lblAwayTeam";
            this.lblAwayTeam.Size = new System.Drawing.Size(89, 16);
            this.lblAwayTeam.TabIndex = 12;
            this.lblAwayTeam.Text = "Away Team";
            // 
            // radioEditMatch
            // 
            this.radioEditMatch.AutoSize = true;
            this.radioEditMatch.Location = new System.Drawing.Point(113, 225);
            this.radioEditMatch.Name = "radioEditMatch";
            this.radioEditMatch.Size = new System.Drawing.Size(107, 17);
            this.radioEditMatch.TabIndex = 23;
            this.radioEditMatch.TabStop = true;
            this.radioEditMatch.Text = "Edit Match Score";
            this.radioEditMatch.UseVisualStyleBackColor = true;
            this.radioEditMatch.CheckedChanged += new System.EventHandler(this.radioEditMatch_CheckedChanged);
            // 
            // radioResetMatch
            // 
            this.radioResetMatch.AutoSize = true;
            this.radioResetMatch.Location = new System.Drawing.Point(113, 248);
            this.radioResetMatch.Name = "radioResetMatch";
            this.radioResetMatch.Size = new System.Drawing.Size(86, 17);
            this.radioResetMatch.TabIndex = 24;
            this.radioResetMatch.TabStop = true;
            this.radioResetMatch.Text = "Reset Match";
            this.radioResetMatch.UseVisualStyleBackColor = true;
            this.radioResetMatch.CheckedChanged += new System.EventHandler(this.radioResetMatch_CheckedChanged);
            // 
            // radioDeleteMatch
            // 
            this.radioDeleteMatch.AutoSize = true;
            this.radioDeleteMatch.Location = new System.Drawing.Point(113, 272);
            this.radioDeleteMatch.Name = "radioDeleteMatch";
            this.radioDeleteMatch.Size = new System.Drawing.Size(89, 17);
            this.radioDeleteMatch.TabIndex = 25;
            this.radioDeleteMatch.TabStop = true;
            this.radioDeleteMatch.Text = "Delete Match";
            this.radioDeleteMatch.UseVisualStyleBackColor = true;
            // 
            // chkDateTime
            // 
            this.chkDateTime.AutoSize = true;
            this.chkDateTime.Location = new System.Drawing.Point(97, 156);
            this.chkDateTime.Name = "chkDateTime";
            this.chkDateTime.Size = new System.Drawing.Size(135, 17);
            this.chkDateTime.TabIndex = 29;
            this.chkDateTime.Text = "Select Date and Time?";
            this.chkDateTime.UseVisualStyleBackColor = true;
            this.chkDateTime.CheckedChanged += new System.EventHandler(this.chkDateTime_CheckedChanged);
            // 
            // matchTimePicker
            // 
            this.matchTimePicker.Location = new System.Drawing.Point(200, 179);
            this.matchTimePicker.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.matchTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.matchTimePicker.Name = "matchTimePicker";
            this.matchTimePicker.Size = new System.Drawing.Size(60, 20);
            this.matchTimePicker.TabIndex = 28;
            this.matchTimePicker.Value = new System.DateTime(2015, 11, 15, 0, 0, 0, 0);
            // 
            // lblTimeAt
            // 
            this.lblTimeAt.AutoSize = true;
            this.lblTimeAt.Location = new System.Drawing.Point(176, 179);
            this.lblTimeAt.Name = "lblTimeAt";
            this.lblTimeAt.Size = new System.Drawing.Size(18, 13);
            this.lblTimeAt.TabIndex = 27;
            this.lblTimeAt.Text = "@";
            // 
            // matchDateTimePicker
            // 
            this.matchDateTimePicker.CustomFormat = "";
            this.matchDateTimePicker.Location = new System.Drawing.Point(36, 179);
            this.matchDateTimePicker.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.matchDateTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.matchDateTimePicker.Name = "matchDateTimePicker";
            this.matchDateTimePicker.Size = new System.Drawing.Size(134, 20);
            this.matchDateTimePicker.TabIndex = 26;
            this.matchDateTimePicker.Value = new System.DateTime(2015, 11, 17, 0, 0, 0, 0);
            // 
            // EditMatchEditWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 338);
            this.Controls.Add(this.chkDateTime);
            this.Controls.Add(this.matchTimePicker);
            this.Controls.Add(this.lblTimeAt);
            this.Controls.Add(this.matchDateTimePicker);
            this.Controls.Add(this.radioDeleteMatch);
            this.Controls.Add(this.radioResetMatch);
            this.Controls.Add(this.radioEditMatch);
            this.Controls.Add(this.lblHomeTeamName);
            this.Controls.Add(this.lblHomeTeam);
            this.Controls.Add(this.lblAwayTeamName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.numAwayScore);
            this.Controls.Add(this.numHomeScore);
            this.Controls.Add(this.lblAwayTeam);
            this.Name = "EditMatchEditWindow";
            this.Text = "EditMatchEditWindow";
            ((System.ComponentModel.ISupportInitialize)(this.numAwayScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeScore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHomeTeamName;
        private System.Windows.Forms.Label lblHomeTeam;
        private System.Windows.Forms.Label lblAwayTeamName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.NumericUpDown numAwayScore;
        private System.Windows.Forms.NumericUpDown numHomeScore;
        private System.Windows.Forms.Label lblAwayTeam;
        private System.Windows.Forms.RadioButton radioEditMatch;
        private System.Windows.Forms.RadioButton radioResetMatch;
        private System.Windows.Forms.RadioButton radioDeleteMatch;
        private System.Windows.Forms.CheckBox chkDateTime;
        private System.Windows.Forms.DateTimePicker matchTimePicker;
        private System.Windows.Forms.Label lblTimeAt;
        private System.Windows.Forms.DateTimePicker matchDateTimePicker;
    }
}