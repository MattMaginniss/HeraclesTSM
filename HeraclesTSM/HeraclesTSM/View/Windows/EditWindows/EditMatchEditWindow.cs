﻿using System;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.EditWindows
{
    public partial class EditMatchEditWindow : Form
    {
        #region Data members

        private readonly Season tournament;
        private readonly int matchIndex;

        #endregion

        #region Constructors

        public EditMatchEditWindow(Season tournament, int matchIndex)
        {
            this.InitializeComponent();
            this.tournament = tournament;
            this.matchIndex = matchIndex;

            this.lblHomeTeamName.Text = this.tournament.GetMatch(this.matchIndex).HomeTeam.Name;
            this.lblAwayTeamName.Text = this.tournament.GetMatch(this.matchIndex).AwayTeam.Name;

            this.numHomeScore.Value = this.tournament.GetMatch(this.matchIndex).HomeTeamScore;
            this.numAwayScore.Value = this.tournament.GetMatch(this.matchIndex).AwayTeamScore;

            this.chkDateTime.Enabled = false;

            this.matchTimePicker.Format = DateTimePickerFormat.Custom;
            this.matchTimePicker.CustomFormat = @"HH:mm";
            this.matchTimePicker.ShowUpDown = true;

            this.matchDateTimePicker.Visible = false;
            this.matchTimePicker.Visible = false;
            this.lblTimeAt.Visible = false;
        }

        #endregion

        private void btnAccept_Click_1(object sender, EventArgs e)
        {
            if (this.radioEditMatch.Checked)
            {
                this.tournament.GetMatch(this.matchIndex).ResetMatch();
                this.tournament.GetMatch(this.matchIndex)
                    .InputGameScore((int) this.numHomeScore.Value, (int) this.numAwayScore.Value);
            }
            else if (this.radioResetMatch.Checked)
            {
                this.tournament.GetMatch(this.matchIndex).ResetMatch();
                if (this.chkDateTime.Checked)
                {
                    var matchDateTime = new DateTime(this.matchDateTimePicker.Value.Year,
                        this.matchDateTimePicker.Value.Month, this.matchDateTimePicker.Value.Day,
                        this.matchTimePicker.Value.Hour, this.matchTimePicker.Value.Minute, 00);

                    this.tournament.GetMatch(this.matchIndex).SetDate(matchDateTime);
                }
            }
            else if (this.radioDeleteMatch.Checked)
            {
                var confirmationWindow = new ConfirmationWindow();
                confirmationWindow.ShowDialog();
                if (confirmationWindow.DialogResult == DialogResult.Yes)
                {
                    this.tournament.DeleteMatch(this.matchIndex);
                }
                confirmationWindow.Close();
            }

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void radioEditMatch_CheckedChanged(object sender, EventArgs e)
        {
            this.numHomeScore.Enabled = true;
            this.numAwayScore.Enabled = true;
        }

        private void radioResetMatch_CheckedChanged(object sender, EventArgs e)
        {
            this.chkDateTime.Enabled = true;

            this.numHomeScore.Value = 0;
            this.numAwayScore.Value = 0;
            this.numHomeScore.Enabled = false;
            this.numAwayScore.Enabled = false;
        }

        private void chkDateTime_CheckedChanged(object sender, EventArgs e)
        {
            if (this.matchDateTimePicker.Visible)
            {
                this.matchDateTimePicker.Visible = false;
                this.matchTimePicker.Visible = false;
                this.lblTimeAt.Visible = false;
            }
            else
            {
                this.matchDateTimePicker.Visible = true;
                this.matchTimePicker.Visible = true;
                this.lblTimeAt.Visible = true;
            }
        }
    }
}