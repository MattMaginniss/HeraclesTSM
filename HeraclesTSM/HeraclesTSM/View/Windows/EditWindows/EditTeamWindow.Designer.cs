﻿namespace HeraclesTSM.View.Windows.EditWindows
{
    partial class EditTeamWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drpDwnTeamName = new System.Windows.Forms.ComboBox();
            this.lblEditTeam = new System.Windows.Forms.Label();
            this.lblEnterTeamName = new System.Windows.Forms.Label();
            this.txtNewTeamName = new System.Windows.Forms.TextBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // drpDwnTeamName
            // 
            this.drpDwnTeamName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpDwnTeamName.FormattingEnabled = true;
            this.drpDwnTeamName.Location = new System.Drawing.Point(149, 49);
            this.drpDwnTeamName.Name = "drpDwnTeamName";
            this.drpDwnTeamName.Size = new System.Drawing.Size(121, 21);
            this.drpDwnTeamName.TabIndex = 0;
            // 
            // lblEditTeam
            // 
            this.lblEditTeam.AutoSize = true;
            this.lblEditTeam.Location = new System.Drawing.Point(57, 52);
            this.lblEditTeam.Name = "lblEditTeam";
            this.lblEditTeam.Size = new System.Drawing.Size(67, 13);
            this.lblEditTeam.TabIndex = 1;
            this.lblEditTeam.Text = "Team to Edit";
            // 
            // lblEnterTeamName
            // 
            this.lblEnterTeamName.AutoSize = true;
            this.lblEnterTeamName.Location = new System.Drawing.Point(60, 113);
            this.lblEnterTeamName.Name = "lblEnterTeamName";
            this.lblEnterTeamName.Size = new System.Drawing.Size(91, 13);
            this.lblEnterTeamName.TabIndex = 2;
            this.lblEnterTeamName.Text = "Enter New Name:";
            // 
            // txtNewTeamName
            // 
            this.txtNewTeamName.Location = new System.Drawing.Point(149, 110);
            this.txtNewTeamName.Name = "txtNewTeamName";
            this.txtNewTeamName.Size = new System.Drawing.Size(121, 20);
            this.txtNewTeamName.TabIndex = 3;
            // 
            // btnEnter
            // 
            this.btnEnter.Location = new System.Drawing.Point(60, 164);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(75, 23);
            this.btnEnter.TabIndex = 4;
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(195, 164);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // EditTeamWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 233);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.txtNewTeamName);
            this.Controls.Add(this.lblEnterTeamName);
            this.Controls.Add(this.lblEditTeam);
            this.Controls.Add(this.drpDwnTeamName);
            this.Name = "EditTeamWindow";
            this.Text = "Edit Team";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox drpDwnTeamName;
        private System.Windows.Forms.Label lblEditTeam;
        private System.Windows.Forms.Label lblEnterTeamName;
        private System.Windows.Forms.TextBox txtNewTeamName;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Button btnCancel;
    }
}