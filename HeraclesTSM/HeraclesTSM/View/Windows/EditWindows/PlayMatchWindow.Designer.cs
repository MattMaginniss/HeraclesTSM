﻿namespace HeraclesTSM.View.Windows.EditWindows
{
    partial class PlayMatchWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAwayTeam = new System.Windows.Forms.Label();
            this.numHomeScore = new System.Windows.Forms.NumericUpDown();
            this.numAwayScore = new System.Windows.Forms.NumericUpDown();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAwayTeamName = new System.Windows.Forms.Label();
            this.lblHomeTeamName = new System.Windows.Forms.Label();
            this.lblHomeTeam = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numHomeScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAwayScore)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAwayTeam
            // 
            this.lblAwayTeam.AutoSize = true;
            this.lblAwayTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblAwayTeam.Location = new System.Drawing.Point(44, 43);
            this.lblAwayTeam.Name = "lblAwayTeam";
            this.lblAwayTeam.Size = new System.Drawing.Size(91, 16);
            this.lblAwayTeam.TabIndex = 1;
            this.lblAwayTeam.Text = "Away Team";
            // 
            // numHomeScore
            // 
            this.numHomeScore.Location = new System.Drawing.Point(242, 115);
            this.numHomeScore.Name = "numHomeScore";
            this.numHomeScore.Size = new System.Drawing.Size(53, 20);
            this.numHomeScore.TabIndex = 4;
            // 
            // numAwayScore
            // 
            this.numAwayScore.Location = new System.Drawing.Point(242, 55);
            this.numAwayScore.Name = "numAwayScore";
            this.numAwayScore.Size = new System.Drawing.Size(53, 20);
            this.numAwayScore.TabIndex = 5;
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(47, 168);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(92, 23);
            this.btnAccept.TabIndex = 6;
            this.btnAccept.Text = "Submit";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(224, 167);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAwayTeamName
            // 
            this.lblAwayTeamName.AutoSize = true;
            this.lblAwayTeamName.Location = new System.Drawing.Point(47, 63);
            this.lblAwayTeamName.Name = "lblAwayTeamName";
            this.lblAwayTeamName.Size = new System.Drawing.Size(0, 13);
            this.lblAwayTeamName.TabIndex = 9;
            // 
            // lblHomeTeamName
            // 
            this.lblHomeTeamName.AutoSize = true;
            this.lblHomeTeamName.Location = new System.Drawing.Point(47, 122);
            this.lblHomeTeamName.Name = "lblHomeTeamName";
            this.lblHomeTeamName.Size = new System.Drawing.Size(0, 13);
            this.lblHomeTeamName.TabIndex = 11;
            // 
            // lblHomeTeam
            // 
            this.lblHomeTeam.AutoSize = true;
            this.lblHomeTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblHomeTeam.Location = new System.Drawing.Point(44, 102);
            this.lblHomeTeam.Name = "lblHomeTeam";
            this.lblHomeTeam.Size = new System.Drawing.Size(95, 16);
            this.lblHomeTeam.TabIndex = 10;
            this.lblHomeTeam.Text = "Home Team";
            // 
            // PlayMatchWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 222);
            this.Controls.Add(this.lblHomeTeamName);
            this.Controls.Add(this.lblHomeTeam);
            this.Controls.Add(this.lblAwayTeamName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.numAwayScore);
            this.Controls.Add(this.numHomeScore);
            this.Controls.Add(this.lblAwayTeam);
            this.Name = "PlayMatchWindow";
            this.Text = "Enter Match Data";
            ((System.ComponentModel.ISupportInitialize)(this.numHomeScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAwayScore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblAwayTeam;
        private System.Windows.Forms.NumericUpDown numHomeScore;
        private System.Windows.Forms.NumericUpDown numAwayScore;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAwayTeamName;
        private System.Windows.Forms.Label lblHomeTeamName;
        private System.Windows.Forms.Label lblHomeTeam;
    }
}