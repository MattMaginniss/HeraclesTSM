﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.CreationWindows
{
    public partial class MatchCreator : Form
    {
        #region Data members

        private readonly Season tournament;

        #endregion

        #region Constructors

        public MatchCreator(Season tournament)
        {
            this.InitializeComponent();
            this.tournament = tournament;
            var teamSource = new Dictionary<int, string>();
            foreach (var team in this.tournament.Teams)
            {
                teamSource.Add(this.tournament.Teams.IndexOf(team), team.Name);
            }
            this.drpdwnHome.DataSource = new BindingSource(teamSource, null);
            this.drpdwnAway.DataSource = new BindingSource(teamSource, null);

            this.drpdwnHome.DisplayMember = "Value";
            this.drpdwnAway.DisplayMember = "Value";

            this.lblTimeAt.Visible = false;
            this.matchDateTimePicker.Visible = false;

            this.matchTimePicker.Visible = false;
            this.matchTimePicker.Format = DateTimePickerFormat.Custom;
            this.matchTimePicker.CustomFormat = @"HH:mm";
            this.matchTimePicker.ShowUpDown = true;

            this.btnCreateMatch.DialogResult = DialogResult.OK;
            this.btnCancel.DialogResult = DialogResult.Cancel;
        }

        #endregion

        private void btnCreateMatch_Click(object sender, EventArgs e)
        {
            Hide();
        }

        public void CreateMatch()
        {
            var homeTeam = this.tournament.Teams[this.drpdwnHome.SelectedIndex];
            var awayTeam = this.tournament.Teams[this.drpdwnAway.SelectedIndex];
            try
            {
                this.tournament.CreateMatch(homeTeam, awayTeam);
                var matchNumber = this.tournament.Matches.Count;

                if (this.chkDateTime.Checked)
                {
                    var matchDateTime = new DateTime(this.matchDateTimePicker.Value.Year,
                        this.matchDateTimePicker.Value.Month, this.matchDateTimePicker.Value.Day,
                        this.matchTimePicker.Value.Hour, this.matchTimePicker.Value.Minute, 00);

                    this.tournament.GetMatch(matchNumber - 1).SetDate(matchDateTime);
                }
            }
            catch (ArgumentException)
            {
                var errorBox = MessageBox.Show("You cannot have " + this.tournament.Teams[this.drpdwnHome.SelectedIndex].Name + " play itself.");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void chkDateTime_CheckedChanged(object sender, EventArgs e)
        {
            if (this.matchDateTimePicker.Visible)
            {
                this.matchDateTimePicker.Visible = false;
                this.matchTimePicker.Visible = false;
                this.lblTimeAt.Visible = false;
            }
            else
            {
                this.matchDateTimePicker.Visible = true;
                this.matchTimePicker.Visible = true;
                this.lblTimeAt.Visible = true;
            }
        }
    }
}