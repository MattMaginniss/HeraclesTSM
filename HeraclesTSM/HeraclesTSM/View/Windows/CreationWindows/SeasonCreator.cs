﻿using System;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.CreationWindows
{
    public partial class SeasonCreator : Form
    {
        #region Constructors

        public SeasonCreator()
        {
            this.InitializeComponent();

            this.btnCreate.DialogResult = DialogResult.OK;
            this.btnCancel.DialogResult = DialogResult.Cancel;
        }

        #endregion

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Hide();
        }

        public string GetSeasonName()
        {
            return this.txtSeasonName.Text;
        }

        public Season CreateSeason()
        {
            Season season = null;

            if (!this.GetSeasonName().Equals(""))
            {
                season = new Season(this.GetSeasonName());
            }

            return season;
        }

        public void ClearData()
        {
            this.txtSeasonName.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}