﻿namespace HeraclesTSM.View.Windows.CreationWindows
{
    partial class MatchCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drpdwnHome = new System.Windows.Forms.ComboBox();
            this.drpdwnAway = new System.Windows.Forms.ComboBox();
            this.btnCreateMatch = new System.Windows.Forms.Button();
            this.lblHomeTeam = new System.Windows.Forms.Label();
            this.lblAwayTeam = new System.Windows.Forms.Label();
            this.lblMatchCreator = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.matchDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblTimeAt = new System.Windows.Forms.Label();
            this.matchTimePicker = new System.Windows.Forms.DateTimePicker();
            this.chkDateTime = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // drpdwnHome
            // 
            this.drpdwnHome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpdwnHome.FormattingEnabled = true;
            this.drpdwnHome.Location = new System.Drawing.Point(12, 104);
            this.drpdwnHome.Name = "drpdwnHome";
            this.drpdwnHome.Size = new System.Drawing.Size(121, 21);
            this.drpdwnHome.TabIndex = 0;
            // 
            // drpdwnAway
            // 
            this.drpdwnAway.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpdwnAway.FormattingEnabled = true;
            this.drpdwnAway.Location = new System.Drawing.Point(151, 104);
            this.drpdwnAway.Name = "drpdwnAway";
            this.drpdwnAway.Size = new System.Drawing.Size(121, 21);
            this.drpdwnAway.TabIndex = 1;
            // 
            // btnCreateMatch
            // 
            this.btnCreateMatch.Location = new System.Drawing.Point(32, 245);
            this.btnCreateMatch.Name = "btnCreateMatch";
            this.btnCreateMatch.Size = new System.Drawing.Size(89, 23);
            this.btnCreateMatch.TabIndex = 5;
            this.btnCreateMatch.Text = "Create Match";
            this.btnCreateMatch.UseVisualStyleBackColor = true;
            this.btnCreateMatch.Click += new System.EventHandler(this.btnCreateMatch_Click);
            // 
            // lblHomeTeam
            // 
            this.lblHomeTeam.AutoSize = true;
            this.lblHomeTeam.Location = new System.Drawing.Point(42, 88);
            this.lblHomeTeam.Name = "lblHomeTeam";
            this.lblHomeTeam.Size = new System.Drawing.Size(65, 13);
            this.lblHomeTeam.TabIndex = 3;
            this.lblHomeTeam.Text = "Home Team";
            // 
            // lblAwayTeam
            // 
            this.lblAwayTeam.AutoSize = true;
            this.lblAwayTeam.Location = new System.Drawing.Point(180, 88);
            this.lblAwayTeam.Name = "lblAwayTeam";
            this.lblAwayTeam.Size = new System.Drawing.Size(63, 13);
            this.lblAwayTeam.TabIndex = 4;
            this.lblAwayTeam.Text = "Away Team";
            // 
            // lblMatchCreator
            // 
            this.lblMatchCreator.AutoSize = true;
            this.lblMatchCreator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatchCreator.Location = new System.Drawing.Point(28, 33);
            this.lblMatchCreator.Name = "lblMatchCreator";
            this.lblMatchCreator.Size = new System.Drawing.Size(230, 20);
            this.lblMatchCreator.TabIndex = 5;
            this.lblMatchCreator.Text = "Select Teams for the Match";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(169, 245);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // matchDateTimePicker
            // 
            this.matchDateTimePicker.CustomFormat = "";
            this.matchDateTimePicker.Location = new System.Drawing.Point(12, 193);
            this.matchDateTimePicker.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.matchDateTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.matchDateTimePicker.Name = "matchDateTimePicker";
            this.matchDateTimePicker.Size = new System.Drawing.Size(134, 20);
            this.matchDateTimePicker.TabIndex = 2;
            this.matchDateTimePicker.Value = new System.DateTime(2015, 11, 17, 0, 0, 0, 0);
            // 
            // lblTimeAt
            // 
            this.lblTimeAt.AutoSize = true;
            this.lblTimeAt.Location = new System.Drawing.Point(152, 193);
            this.lblTimeAt.Name = "lblTimeAt";
            this.lblTimeAt.Size = new System.Drawing.Size(18, 13);
            this.lblTimeAt.TabIndex = 8;
            this.lblTimeAt.Text = "@";
            // 
            // matchTimePicker
            // 
            this.matchTimePicker.Location = new System.Drawing.Point(176, 193);
            this.matchTimePicker.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.matchTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.matchTimePicker.Name = "matchTimePicker";
            this.matchTimePicker.Size = new System.Drawing.Size(60, 20);
            this.matchTimePicker.TabIndex = 9;
            this.matchTimePicker.Value = new System.DateTime(2015, 11, 15, 0, 0, 0, 0);
            // 
            // chkDateTime
            // 
            this.chkDateTime.AutoSize = true;
            this.chkDateTime.Location = new System.Drawing.Point(73, 170);
            this.chkDateTime.Name = "chkDateTime";
            this.chkDateTime.Size = new System.Drawing.Size(135, 17);
            this.chkDateTime.TabIndex = 10;
            this.chkDateTime.Text = "Select Date and Time?";
            this.chkDateTime.UseVisualStyleBackColor = true;
            this.chkDateTime.CheckedChanged += new System.EventHandler(this.chkDateTime_CheckedChanged);
            // 
            // MatchCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 285);
            this.Controls.Add(this.chkDateTime);
            this.Controls.Add(this.matchTimePicker);
            this.Controls.Add(this.lblTimeAt);
            this.Controls.Add(this.matchDateTimePicker);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMatchCreator);
            this.Controls.Add(this.lblAwayTeam);
            this.Controls.Add(this.lblHomeTeam);
            this.Controls.Add(this.btnCreateMatch);
            this.Controls.Add(this.drpdwnAway);
            this.Controls.Add(this.drpdwnHome);
            this.Name = "MatchCreator";
            this.Text = "MatchCreator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox drpdwnHome;
        private System.Windows.Forms.ComboBox drpdwnAway;
        private System.Windows.Forms.Button btnCreateMatch;
        private System.Windows.Forms.Label lblHomeTeam;
        private System.Windows.Forms.Label lblAwayTeam;
        private System.Windows.Forms.Label lblMatchCreator;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DateTimePicker matchDateTimePicker;
        private System.Windows.Forms.Label lblTimeAt;
        private System.Windows.Forms.DateTimePicker matchTimePicker;
        private System.Windows.Forms.CheckBox chkDateTime;
    }
}