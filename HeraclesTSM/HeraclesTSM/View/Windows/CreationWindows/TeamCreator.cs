﻿using System;
using System.Windows.Forms;
using HeraclesTSM.Model;

namespace HeraclesTSM.View.Windows.CreationWindows
{
    public partial class TeamCreator : Form
    {
        #region Constructors

        public TeamCreator()
        {
            this.InitializeComponent();

            this.btnCreate.DialogResult = DialogResult.OK;
            this.btnCancel.DialogResult = DialogResult.Cancel;
        }

        #endregion

        public string GetTeamName()
        {
            return this.textBox1.Text;
        }

        public void ClearData()
        {
            this.textBox1.Clear();
        }

        public Team CreateTeam()
        {
            var teamName = this.GetTeamName();

            var team = new Team(teamName);

            return team;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}