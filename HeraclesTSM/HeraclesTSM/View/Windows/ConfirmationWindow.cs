﻿using System;
using System.Windows.Forms;

namespace HeraclesTSM.View.Windows
{
    public partial class ConfirmationWindow : Form
    {
        #region Constructors

        public ConfirmationWindow()
        {
            this.InitializeComponent();
        }

        #endregion

        private void btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }
    }
}