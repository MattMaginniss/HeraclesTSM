﻿using System.Windows.Forms;

namespace HeraclesTSM.View.Windows
{
    partial class PrimaryWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTournamentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTournamentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTournamentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsTournamentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seasonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTeamMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMatchMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTeamMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMatchMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabHome = new System.Windows.Forms.TabPage();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.tabTeams = new System.Windows.Forms.TabPage();
            this.lblTeamName = new System.Windows.Forms.Label();
            this.dgvTeams = new System.Windows.Forms.DataGridView();
            this.clmnTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabStandings = new System.Windows.Forms.TabPage();
            this.dgvStandings = new System.Windows.Forms.DataGridView();
            this.TeamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GamesPlayed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Points = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Wins = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ties = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Loss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PointDifference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabMatches = new System.Windows.Forms.TabPage();
            this.dgvMatches = new System.Windows.Forms.DataGridView();
            this.MatchNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HomeTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AwayTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabFixtures = new System.Windows.Forms.TabPage();
            this.dgvFixtures = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.dgvResults = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabHome.SuspendLayout();
            this.tabTeams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeams)).BeginInit();
            this.tabStandings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStandings)).BeginInit();
            this.tabMatches.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatches)).BeginInit();
            this.tabFixtures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixtures)).BeginInit();
            this.tabResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.seasonToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(890, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTournamentMenuItem,
            this.openTournamentMenuItem,
            this.saveTournamentMenuItem,
            this.saveAsTournamentMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newTournamentMenuItem
            // 
            this.newTournamentMenuItem.Name = "newTournamentMenuItem";
            this.newTournamentMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.N)));
            this.newTournamentMenuItem.Size = new System.Drawing.Size(177, 22);
            this.newTournamentMenuItem.Text = "New";
            this.newTournamentMenuItem.Click += new System.EventHandler(this.newTournamentToolStripMenuItem_Click);
            // 
            // openTournamentMenuItem
            // 
            this.openTournamentMenuItem.Name = "openTournamentMenuItem";
            this.openTournamentMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openTournamentMenuItem.Size = new System.Drawing.Size(177, 22);
            this.openTournamentMenuItem.Text = "Open";
            this.openTournamentMenuItem.Click += new System.EventHandler(this.openTournamentMenuItem_Click);
            // 
            // saveTournamentMenuItem
            // 
            this.saveTournamentMenuItem.Name = "saveTournamentMenuItem";
            this.saveTournamentMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveTournamentMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveTournamentMenuItem.Text = "Save";
            this.saveTournamentMenuItem.Click += new System.EventHandler(this.saveTournamentMenuItem_Click);
            // 
            // saveAsTournamentMenuItem
            // 
            this.saveAsTournamentMenuItem.Name = "saveAsTournamentMenuItem";
            this.saveAsTournamentMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsTournamentMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveAsTournamentMenuItem.Text = "Save As";
            this.saveAsTournamentMenuItem.Click += new System.EventHandler(this.saveAsTournament_Click);
            // 
            // seasonToolStripMenuItem
            // 
            this.seasonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.editToolStripMenuItem});
            this.seasonToolStripMenuItem.Name = "seasonToolStripMenuItem";
            this.seasonToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.seasonToolStripMenuItem.Text = "&Edit";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTeamMenuItem,
            this.addMatchMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // addTeamMenuItem
            // 
            this.addTeamMenuItem.Name = "addTeamMenuItem";
            this.addTeamMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.addTeamMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addTeamMenuItem.Text = "Team";
            this.addTeamMenuItem.Click += new System.EventHandler(this.newTeamToolStripMenuItem1_Click);
            // 
            // addMatchMenuItem
            // 
            this.addMatchMenuItem.Name = "addMatchMenuItem";
            this.addMatchMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.addMatchMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addMatchMenuItem.Text = "Match";
            this.addMatchMenuItem.Click += new System.EventHandler(this.addMatchMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editTeamMenuItem,
            this.editMatchMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // editTeamMenuItem
            // 
            this.editTeamMenuItem.Name = "editTeamMenuItem";
            this.editTeamMenuItem.Size = new System.Drawing.Size(108, 22);
            this.editTeamMenuItem.Text = "Team";
            this.editTeamMenuItem.Click += new System.EventHandler(this.editTeamMenuItem_Click);
            // 
            // editMatchMenuItem
            // 
            this.editMatchMenuItem.Name = "editMatchMenuItem";
            this.editMatchMenuItem.Size = new System.Drawing.Size(108, 22);
            this.editMatchMenuItem.Text = "Match";
            this.editMatchMenuItem.Click += new System.EventHandler(this.editMatchMenuItem_Click);
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabHome);
            this.tabs.Controls.Add(this.tabTeams);
            this.tabs.Controls.Add(this.tabStandings);
            this.tabs.Controls.Add(this.tabMatches);
            this.tabs.Controls.Add(this.tabFixtures);
            this.tabs.Controls.Add(this.tabResults);
            this.tabs.Location = new System.Drawing.Point(0, 28);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(890, 467);
            this.tabs.TabIndex = 1;
            // 
            // tabHome
            // 
            this.tabHome.Controls.Add(this.lblWelcome);
            this.tabHome.Location = new System.Drawing.Point(4, 22);
            this.tabHome.Name = "tabHome";
            this.tabHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabHome.Size = new System.Drawing.Size(882, 441);
            this.tabHome.TabIndex = 0;
            this.tabHome.Text = "Home";
            this.tabHome.UseVisualStyleBackColor = true;
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(25, 23);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(483, 22);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome to Heracles Tournament Season Manager";
            // 
            // tabTeams
            // 
            this.tabTeams.Controls.Add(this.lblTeamName);
            this.tabTeams.Controls.Add(this.dgvTeams);
            this.tabTeams.Location = new System.Drawing.Point(4, 22);
            this.tabTeams.Name = "tabTeams";
            this.tabTeams.Padding = new System.Windows.Forms.Padding(3);
            this.tabTeams.Size = new System.Drawing.Size(882, 441);
            this.tabTeams.TabIndex = 3;
            this.tabTeams.Text = "Teams";
            this.tabTeams.UseVisualStyleBackColor = true;
            // 
            // lblTeamName
            // 
            this.lblTeamName.AutoSize = true;
            this.lblTeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeamName.Location = new System.Drawing.Point(424, 27);
            this.lblTeamName.Name = "lblTeamName";
            this.lblTeamName.Size = new System.Drawing.Size(0, 33);
            this.lblTeamName.TabIndex = 1;
            // 
            // dgvTeams
            // 
            this.dgvTeams.AllowUserToAddRows = false;
            this.dgvTeams.AllowUserToDeleteRows = false;
            this.dgvTeams.AllowUserToResizeColumns = false;
            this.dgvTeams.AllowUserToResizeRows = false;
            this.dgvTeams.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTeams.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvTeams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTeams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnTeam});
            this.dgvTeams.Location = new System.Drawing.Point(7, 7);
            this.dgvTeams.Name = "dgvTeams";
            this.dgvTeams.RowHeadersVisible = false;
            this.dgvTeams.Size = new System.Drawing.Size(203, 360);
            this.dgvTeams.TabIndex = 0;
            this.dgvTeams.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTeams_CellContentClick);
            // 
            // clmnTeam
            // 
            this.clmnTeam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmnTeam.HeaderText = "Teams";
            this.clmnTeam.Name = "clmnTeam";
            this.clmnTeam.ReadOnly = true;
            this.clmnTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabStandings
            // 
            this.tabStandings.Controls.Add(this.dgvStandings);
            this.tabStandings.Location = new System.Drawing.Point(4, 22);
            this.tabStandings.Name = "tabStandings";
            this.tabStandings.Padding = new System.Windows.Forms.Padding(3);
            this.tabStandings.Size = new System.Drawing.Size(882, 441);
            this.tabStandings.TabIndex = 1;
            this.tabStandings.Text = "Standings";
            this.tabStandings.UseVisualStyleBackColor = true;
            // 
            // dgvStandings
            // 
            this.dgvStandings.AllowUserToAddRows = false;
            this.dgvStandings.AllowUserToDeleteRows = false;
            this.dgvStandings.AllowUserToResizeColumns = false;
            this.dgvStandings.AllowUserToResizeRows = false;
            this.dgvStandings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStandings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvStandings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStandings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TeamName,
            this.GamesPlayed,
            this.Points,
            this.Wins,
            this.Ties,
            this.Loss,
            this.PointDifference});
            this.dgvStandings.Location = new System.Drawing.Point(7, 7);
            this.dgvStandings.Name = "dgvStandings";
            this.dgvStandings.RowHeadersVisible = false;
            this.dgvStandings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvStandings.Size = new System.Drawing.Size(379, 360);
            this.dgvStandings.TabIndex = 0;
            // 
            // TeamName
            // 
            this.TeamName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TeamName.HeaderText = "Name";
            this.TeamName.Name = "TeamName";
            this.TeamName.ReadOnly = true;
            this.TeamName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TeamName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TeamName.Width = 41;
            // 
            // GamesPlayed
            // 
            this.GamesPlayed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GamesPlayed.DefaultCellStyle = dataGridViewCellStyle17;
            this.GamesPlayed.FillWeight = 99.60552F;
            this.GamesPlayed.HeaderText = "GP";
            this.GamesPlayed.Name = "GamesPlayed";
            this.GamesPlayed.ReadOnly = true;
            this.GamesPlayed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GamesPlayed.Width = 35;
            // 
            // Points
            // 
            this.Points.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Points.DefaultCellStyle = dataGridViewCellStyle18;
            this.Points.FillWeight = 211.5384F;
            this.Points.HeaderText = "Points";
            this.Points.Name = "Points";
            this.Points.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Points.Width = 55;
            // 
            // Wins
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Wins.DefaultCellStyle = dataGridViewCellStyle19;
            this.Wins.FillWeight = 67.95428F;
            this.Wins.HeaderText = "W";
            this.Wins.Name = "Wins";
            this.Wins.ReadOnly = true;
            this.Wins.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Ties
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Ties.DefaultCellStyle = dataGridViewCellStyle20;
            this.Ties.FillWeight = 66.39336F;
            this.Ties.HeaderText = "T";
            this.Ties.Name = "Ties";
            this.Ties.ReadOnly = true;
            this.Ties.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Loss
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Loss.DefaultCellStyle = dataGridViewCellStyle21;
            this.Loss.FillWeight = 65.05779F;
            this.Loss.HeaderText = "L";
            this.Loss.Name = "Loss";
            this.Loss.ReadOnly = true;
            this.Loss.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PointDifference
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PointDifference.DefaultCellStyle = dataGridViewCellStyle22;
            this.PointDifference.FillWeight = 89.45057F;
            this.PointDifference.HeaderText = "PD";
            this.PointDifference.Name = "PointDifference";
            this.PointDifference.ReadOnly = true;
            this.PointDifference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabMatches
            // 
            this.tabMatches.Controls.Add(this.dgvMatches);
            this.tabMatches.Location = new System.Drawing.Point(4, 22);
            this.tabMatches.Name = "tabMatches";
            this.tabMatches.Padding = new System.Windows.Forms.Padding(3);
            this.tabMatches.Size = new System.Drawing.Size(882, 441);
            this.tabMatches.TabIndex = 2;
            this.tabMatches.Text = "Matches";
            this.tabMatches.UseVisualStyleBackColor = true;
            // 
            // dgvMatches
            // 
            this.dgvMatches.AllowUserToAddRows = false;
            this.dgvMatches.AllowUserToDeleteRows = false;
            this.dgvMatches.AllowUserToResizeColumns = false;
            this.dgvMatches.AllowUserToResizeRows = false;
            this.dgvMatches.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMatches.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvMatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMatches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MatchNumber,
            this.HomeTeam,
            this.ResultTime,
            this.AwayTeam});
            this.dgvMatches.Location = new System.Drawing.Point(7, 7);
            this.dgvMatches.Name = "dgvMatches";
            this.dgvMatches.RowHeadersVisible = false;
            this.dgvMatches.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMatches.Size = new System.Drawing.Size(354, 360);
            this.dgvMatches.TabIndex = 0;
            this.dgvMatches.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMatches_CellContentClick);
            // 
            // MatchNumber
            // 
            this.MatchNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MatchNumber.FillWeight = 54.87805F;
            this.MatchNumber.HeaderText = "#";
            this.MatchNumber.Name = "MatchNumber";
            this.MatchNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MatchNumber.Width = 25;
            // 
            // HomeTeam
            // 
            this.HomeTeam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.HomeTeam.FillWeight = 122.561F;
            this.HomeTeam.HeaderText = "Home Team";
            this.HomeTeam.Name = "HomeTeam";
            this.HomeTeam.ReadOnly = true;
            this.HomeTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ResultTime
            // 
            this.ResultTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ResultTime.DefaultCellStyle = dataGridViewCellStyle24;
            this.ResultTime.HeaderText = "Result/Time";
            this.ResultTime.Name = "ResultTime";
            this.ResultTime.ReadOnly = true;
            this.ResultTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ResultTime.Width = 115;
            // 
            // AwayTeam
            // 
            this.AwayTeam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AwayTeam.FillWeight = 122.561F;
            this.AwayTeam.HeaderText = "Away Team";
            this.AwayTeam.Name = "AwayTeam";
            this.AwayTeam.ReadOnly = true;
            this.AwayTeam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabFixtures
            // 
            this.tabFixtures.Controls.Add(this.dgvFixtures);
            this.tabFixtures.Location = new System.Drawing.Point(4, 22);
            this.tabFixtures.Name = "tabFixtures";
            this.tabFixtures.Padding = new System.Windows.Forms.Padding(3);
            this.tabFixtures.Size = new System.Drawing.Size(882, 441);
            this.tabFixtures.TabIndex = 4;
            this.tabFixtures.Text = "Fixtures";
            this.tabFixtures.UseVisualStyleBackColor = true;
            // 
            // dgvFixtures
            // 
            this.dgvFixtures.AllowUserToAddRows = false;
            this.dgvFixtures.AllowUserToDeleteRows = false;
            this.dgvFixtures.AllowUserToResizeColumns = false;
            this.dgvFixtures.AllowUserToResizeRows = false;
            this.dgvFixtures.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFixtures.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvFixtures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFixtures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgvFixtures.Location = new System.Drawing.Point(7, 7);
            this.dgvFixtures.Name = "dgvFixtures";
            this.dgvFixtures.RowHeadersVisible = false;
            this.dgvFixtures.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFixtures.Size = new System.Drawing.Size(354, 360);
            this.dgvFixtures.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.FillWeight = 54.87805F;
            this.dataGridViewTextBoxColumn1.HeaderText = "#";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.FillWeight = 122.561F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Home Team";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn3.HeaderText = "Time";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 115;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.FillWeight = 122.561F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Away Team";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.dgvResults);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(882, 441);
            this.tabResults.TabIndex = 5;
            this.tabResults.Text = "Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // dgvResults
            // 
            this.dgvResults.AllowUserToAddRows = false;
            this.dgvResults.AllowUserToDeleteRows = false;
            this.dgvResults.AllowUserToResizeColumns = false;
            this.dgvResults.AllowUserToResizeRows = false;
            this.dgvResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgvResults.Location = new System.Drawing.Point(7, 7);
            this.dgvResults.Name = "dgvResults";
            this.dgvResults.RowHeadersVisible = false;
            this.dgvResults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvResults.Size = new System.Drawing.Size(354, 360);
            this.dgvResults.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.FillWeight = 54.87805F;
            this.dataGridViewTextBoxColumn5.HeaderText = "#";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 25;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.FillWeight = 122.561F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Home Team";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn7.HeaderText = "Result";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.FillWeight = 122.561F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Away Team";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PrimaryWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 495);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PrimaryWindow";
            this.Text = "Heracles TSM";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabs.ResumeLayout(false);
            this.tabHome.ResumeLayout(false);
            this.tabHome.PerformLayout();
            this.tabTeams.ResumeLayout(false);
            this.tabTeams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeams)).EndInit();
            this.tabStandings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStandings)).EndInit();
            this.tabMatches.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatches)).EndInit();
            this.tabFixtures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixtures)).EndInit();
            this.tabResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTournamentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTournamentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTournamentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seasonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTeamMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addMatchMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsTournamentMenuItem;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabHome;
        private System.Windows.Forms.TabPage tabStandings;
        private System.Windows.Forms.TabPage tabMatches;
        private System.Windows.Forms.TabPage tabTeams;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.DataGridView dgvStandings;
        private System.Windows.Forms.DataGridView dgvMatches;
        private DataGridView dgvTeams;
        private Label lblTeamName;
        private DataGridViewTextBoxColumn clmnTeam;
        private TabPage tabFixtures;
        private TabPage tabResults;
        private DataGridView dgvFixtures;
        private DataGridView dgvResults;
        private DataGridViewTextBoxColumn MatchNumber;
        private DataGridViewTextBoxColumn HomeTeam;
        private DataGridViewTextBoxColumn ResultTime;
        private DataGridViewTextBoxColumn AwayTeam;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn TeamName;
        private DataGridViewTextBoxColumn GamesPlayed;
        private DataGridViewTextBoxColumn Points;
        private DataGridViewTextBoxColumn Wins;
        private DataGridViewTextBoxColumn Ties;
        private DataGridViewTextBoxColumn Loss;
        private DataGridViewTextBoxColumn PointDifference;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem editTeamMenuItem;
        private ToolStripMenuItem editMatchMenuItem;
    }
}