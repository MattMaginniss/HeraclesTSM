﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using HeraclesTSM.Model;
using HeraclesTSM.View.Windows.CreationWindows;
using HeraclesTSM.View.Windows.EditWindows;

namespace HeraclesTSM.View.Windows
{
    public partial class PrimaryWindow : Form
    {
        #region Data members

        private string saveFilePath;

        #region Objects

        private Season tournament;

        #endregion Objects

        #endregion

        #region Constructors

        public PrimaryWindow()
        {
            this.InitializeComponent();
            this.tabs.Dock = DockStyle.Fill;

            this.dgvStandings.Hide();
            this.dgvMatches.Hide();
            this.dgvFixtures.Hide();
            this.dgvResults.Hide();
            this.dgvTeams.Hide();

            this.addToolStripMenuItem.Enabled = false;
            this.addTeamMenuItem.Enabled = false;
            this.addMatchMenuItem.Enabled = false;

            this.editToolStripMenuItem.Enabled = false;
            this.editTeamMenuItem.Enabled = false;
            this.editMatchMenuItem.Enabled = false;

            this.saveTournamentMenuItem.Enabled = false;
            this.saveAsTournamentMenuItem.Enabled = false;
        }

        #endregion

        #region Windows

        private SeasonCreator seasonCreator;
        private TeamCreator teamCreator;
        private MatchCreator matchCreator;

        #endregion Windows

        private void newTournamentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.seasonCreator = new SeasonCreator();
            this.seasonCreator.ShowDialog();

            if (this.seasonCreator.DialogResult == DialogResult.OK &&
                !this.seasonCreator.GetSeasonName().Equals(""))
            {
                this.tournament = this.seasonCreator.CreateSeason();
                this.newTournamentMenuItem.Enabled = false;

                this.enableAllMenuItems();

                this.updateAllDataGridViews();
            }

            this.seasonCreator.Close();
        }

        private void enableAllMenuItems()
        {
            this.addToolStripMenuItem.Enabled = true;
            this.addTeamMenuItem.Enabled = true;
            this.addMatchMenuItem.Enabled = true;

            this.editToolStripMenuItem.Enabled = true;
            this.editTeamMenuItem.Enabled = true;
            this.editMatchMenuItem.Enabled = true;

            this.saveTournamentMenuItem.Enabled = true;
            this.saveAsTournamentMenuItem.Enabled = true;
        }

        private void newTeamToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.teamCreator = new TeamCreator();
            this.teamCreator.ShowDialog();
            if (this.teamCreator.DialogResult == DialogResult.OK && !this.teamCreator.GetTeamName().Equals(""))
            {
                var team = this.teamCreator.CreateTeam();
                try
                {
                    this.tournament.AddTeamToSeason(team);
                }
                catch (ArgumentException)
                {
                    var error = MessageBox.Show("You cannot create a second team with the name " + team.Name);
                }
                this.updateAllDataGridViews();
                this.teamCreator.ClearData();
            }
            this.teamCreator.Close();
        }

        private void updateStandingsDataGridView()
        {
            this.dgvStandings.Rows.Clear();
            this.tournament.SortStandings();
            foreach (var team in this.tournament.TeamStandings)
            {
                var name = team.Name;
                var points = team.Points;
                var gp = team.GamesPlayed;
                var w = team.Wins;
                var t = team.Ties;
                var l = team.Losses;
                var pd = team.PointDifference;
                this.dgvStandings.Rows.Add(name, gp, points, w, t, l, pd);
            }
            this.dgvStandings.Show();
        }

        private void addMatchMenuItem_Click(object sender, EventArgs e)
        {
            this.matchCreator = new MatchCreator(this.tournament);
            this.matchCreator.ShowDialog();
            if (this.matchCreator.DialogResult == DialogResult.OK)
            {
                this.matchCreator.CreateMatch();
                this.dgvMatches.Visible = true;
                this.updateAllDataGridViews();
            }
        }

        private void updateMatchesDataGridView()
        {
            this.dgvMatches.Rows.Clear();

            foreach (var match in this.tournament.Matches)
            {
                var matchNumber = match.MatchNumber;
                var homeTeam = match.HomeTeam.Name;
                var scoreTime = this.setScoreTime(match);
                if (match.IsFinished)
                {
                    scoreTime = match.HomeTeamScore + " - " + match.AwayTeamScore;
                }
                var awayTeam = match.AwayTeam.Name;

                this.dgvMatches.Rows.Add(matchNumber, homeTeam, scoreTime, awayTeam);
            }
            this.dgvMatches.Show();
        }

        private void updateFixturesAndResultDgv()
        {
            this.dgvFixtures.Rows.Clear();
            this.dgvResults.Rows.Clear();

            foreach (var match in this.tournament.Matches)
            {
                var matchNumber = match.MatchNumber;
                var homeTeam = match.HomeTeam.Name;
                var awayTeam = match.AwayTeam.Name;
                var scoreTime = this.setScoreTime(match);
                if (match.IsFinished)
                {
                    scoreTime = match.HomeTeamScore + " - " + match.AwayTeamScore;
                    this.dgvResults.Rows.Add(matchNumber, homeTeam, scoreTime, awayTeam);
                }
                else
                {
                    this.dgvFixtures.Rows.Add(matchNumber, homeTeam, scoreTime, awayTeam);
                }
            }
            this.dgvFixtures.Show();
            this.dgvResults.Show();
        }

        private string setScoreTime(Match match)
        {
            string scoreTime;
            if (match.IsFinished)
            {
                scoreTime = match.HomeTeamScore + " - " + match.AwayTeamScore;
            }
            else if (match.DateTime == DateTime.MinValue)
            {
                scoreTime = "Match Date Not Set";
            }
            else
            {
                scoreTime = match.DateTime.Day + "/" + match.DateTime.Month + "/" + match.DateTime.Year + " @ " +
                            $"{match.DateTime.Hour:00}" + ":" + $"{match.DateTime.Minute:00}";
            }
            return scoreTime;
        }

        private void updateTeamsDataGridView()
        {
            this.dgvTeams.Rows.Clear();

            foreach (var team in this.tournament.Teams)
            {
                this.dgvTeams.Rows.Add(team.Name);
            }
            this.dgvTeams.Show();
        }

        private void saveTournamentMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.saveFilePath))
            {
                this.saveTournamentData(false);
            }
            else
            {
                this.saveTournamentData(true);
            }
        }

        private void saveAsTournament_Click(object sender, EventArgs e)
        {
            this.saveTournamentData(true);
        }

        private void saveTournamentData(bool newFile)
        {
            var saveFileDialog = new SaveFileDialog
            {
                DefaultExt = ".bin",
                Filter = @"Bin Files (*.bin)|*.bin",
                AddExtension = true,
                FileName = this.tournament.Name
            };

            IFormatter formatter = new BinaryFormatter();
            Stream stream;
            if (newFile)
            {
                saveFileDialog.ShowDialog();
                stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            }
            else
            {
                stream = new FileStream(this.saveFilePath, FileMode.Create, FileAccess.Write, FileShare.None);
            }
            this.saveFilePath = saveFileDialog.FileName;
            formatter.Serialize(stream, this.tournament);
            stream.Close();
        }

        private void openTournamentMenuItem_Click(object sender, EventArgs e)
        {
            this.openFile();
            this.displayLoadedData();
        }

        private void displayLoadedData()
        {
            if (this.saveFilePath != "")
            {
                this.updateAllDataGridViews();
                this.enableAllMenuItems();
            }
        }

        private void updateAllDataGridViews()
        {
            this.updateMatchesDataGridView();
            this.updateFixturesAndResultDgv();
            this.updateStandingsDataGridView();
            this.updateTeamsDataGridView();
        }

        private void openFile()
        {
            var openFileDialog = new OpenFileDialog
            {
                DefaultExt = ".bin",
                Filter = @"Bin Files (*.bin)|*.bin"
            };
            openFileDialog.ShowDialog();
            this.saveFilePath = openFileDialog.FileName;
            if (this.saveFilePath != "")
            {
                Stream stream = new FileStream(this.saveFilePath, FileMode.Open, FileAccess.Read, FileShare.None);
                IFormatter formatter = new BinaryFormatter();
                this.tournament = (Season) formatter.Deserialize(stream);
                stream.Close();
            }
        }

        private void dgvTeams_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.lblTeamName.Text = this.tournament.GetTeam(this.dgvTeams.CurrentCell.RowIndex).Name;
        }

        private void dgvMatches_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var matchIndex = this.dgvMatches.CurrentCell.RowIndex;
            var window = new PlayMatchWindow(this.tournament.GetMatch(matchIndex));
            window.ShowDialog();
            if (window.DialogResult == DialogResult.OK)
            {
                this.tournament.GetMatch(matchIndex).InputGameScore(window.HomeTeamScore, window.AwayTeamScore);
                this.updateAllDataGridViews();
            }
            window.Close();
        }

        private void editTeamMenuItem_Click(object sender, EventArgs e)
        {
            var editTeamWindow = new EditTeamWindow(this.tournament);
            editTeamWindow.ShowDialog();
            editTeamWindow.Close();
            this.updateAllDataGridViews();
        }

        private void editMatchMenuItem_Click(object sender, EventArgs e)
        {
            var editMatchWindow = new EditMatchSelectionWindow(this.tournament);
            editMatchWindow.ShowDialog();
            editMatchWindow.Close();
            this.updateAllDataGridViews();
        }
    }
}